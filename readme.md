# Deployer 🚀

## Next Task
- gen schema

## Iterations
### First
1. upload one [resource].xlsx
2. verify follows [resource].schema.json
3. add resource instance to mongodb 
4. deploy resource
### Final 
1. Get deployment token from devops
2. upload all [resource].xlsx
3. verify follows [resource].schema.json
4. add resource instance to mongodb with 
5. deploy resource
6. create user and password in key cloak with mongo `siteId` encoded in jwt
7. upon login decode token and request site resources 
