import asyncio
import logging

import uvicorn
from fastapi import FastAPI, UploadFile
from fastapi.middleware.cors import CORSMiddleware

from src.domain import create_resource
from src.models import Site
from src.utils import load_config, setup_logger

app = FastAPI()
origins = [
    "http://localhost",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


#  name of arg must be the same as the key of the file e.g. formData.append("file", file)
@app.post("/workbook")
async def create(workbook: UploadFile) -> Site:
    contents = await workbook.read()
    return create_resource(contents)


@app.get("/workbook")
async def get(workbook: UploadFile) -> None:
    pass


@app.put("/workbook")
async def update(workbook: UploadFile) -> None:
    pass


@app.delete("/workbook")
async def delete(workbook: UploadFile) -> None:
    pass


#  simplify config so file works
async def main() -> None:
    cfg = await load_config()
    setup_logger(cfg.log_level)
    logging.info(f"Running with: Config( {cfg} )")
    config = uvicorn.Config(
        "src.rest:app",
        port=cfg.port,
        host=str(cfg.host),
        reload=cfg.uvicorn_reload,
        log_level="error",
    )
    server = uvicorn.Server(config)
    await server.serve()


if __name__ == "__main__":
    asyncio.run(main())
