from collections import OrderedDict
from io import BytesIO
from itertools import islice

import openpyxl

from src.models import Site


def create_resource(contents: bytes) -> Site:
    wb = openpyxl.load_workbook(filename=BytesIO(contents))
    sheet = wb.worksheets[0]
    headers = list(sheet.values)[0]
    data = list(sheet.values)[1]
    return Site(**dict(zip(headers, data)))
