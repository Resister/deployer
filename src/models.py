from enum import Enum

from pydantic import BaseModel


class Site(BaseModel):
    name: str
    latitude: int
    longitude: int
