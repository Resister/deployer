from typing import Tuple

import pytest
from fastapi.testclient import TestClient
from pydantic import BaseModel

from src.models import Site
from src.rest import app


class Resources(BaseModel):
    site: Site


async def init() -> Tuple[TestClient, Resources]:
    resources = Resources(site=Site(name="bm2", latitude=90, longitude=-180))
    return TestClient(app), resources


@pytest.mark.asyncio
async def test_create() -> None:
    (client, expected) = await init()
    response = client.post(
        "/workbook",
        files={"workbook": ("./site.xlsx", open("tests/fixtures/site.xlsx", "rb"))},
    )
    assert response.json() == expected.site.dict()
